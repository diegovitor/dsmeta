import Headerdsmeta from "./components/Header/Headerdsmeta";
import NotificationButton from "./components/NotificationButton/NotificationButton";
import SalesCard from "./components/SalesCard/SalesCard";

function App() {
  return (
    <>
      <Headerdsmeta/>
      <main>
        <section id="sales">
          <div className="dsmeta-container">
            <SalesCard/>
          </div>
        </section>
      </main>
    </>
  );
}

export default App;

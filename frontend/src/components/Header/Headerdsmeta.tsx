import logo from '../../assets/img/logo.svg'
import './styles.css'
export default function Headerdsmeta(){
    return(
        <header>
            <div className='dsmeta-logo-container'>
                <img src={logo} alt="Logotipo dsmeta" />
                <h1>DSMeta</h1>
                <p>Desenvolvido por @devsuperior.ig</p>
            </div>
        </header>
    )
}
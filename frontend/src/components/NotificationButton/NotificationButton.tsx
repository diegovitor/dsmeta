import icon from '../../assets/img/notification-icon.svg'
import './styles.css'

export default function NotificationButton() {
  return (
   <div className='dsmeta-red-bnt'>
        <img src={icon} alt="Botão de Notificação" />
   </div>
  );
}

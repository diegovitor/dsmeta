#!/bin/bash
name="${1?one argument needed}"

ids=$(docker ps -a | awk -v name="$name" '$NF ~ name{print $1}')
[[ -n $ids ]] && docker rm -f $ids
